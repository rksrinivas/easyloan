DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `id` varchar(36) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins` (
  `username` varchar(64) DEFAULT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) DEFAULT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
);

DROP TABLE IF EXISTS `Borrower`;
CREATE TABLE `Borrower` (
  `id` varchar(36) NOT NULL,
  `active` bit(1),
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `amount`	int(255) DEFAULT NULL,
  `purpose` varchar(255) DEFAULT NULL,
  `tenure` varchar(255) DEFAULT NULL,
  `days` int(255) DEFAULT NULL,
  `employment` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `residence_type` varchar(255) DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `no_of_dependents` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `current_job_stability` varchar(255) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `funded`  int(255) DEFAULT NULL,
  `remaining_days` int(255) DEFAULT NULL,
  `needed` int(255) DEFAULT NULL,
  `no_of_proposals` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

DROP TABLE IF EXISTS `user_proposal`;
CREATE TABLE `user_proposal` (
  `id` varchar(36) NOT NULL,
  `borrower_id` varchar(255) DEFAULT NULL,
  `lender_id` varchar(255) DEFAULT NULL,
  `proposed_on` datetime,
  `amount` int(255) DEFAULT NULL,
  `roi`  int(255) DEFAULT NULL,
  `status`  varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
