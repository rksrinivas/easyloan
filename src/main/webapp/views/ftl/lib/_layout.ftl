<#macro page>
<#compress>
<header class="header-banner">
  <div class="container-width">
    <div class="logo-container">
      <div class="logo"><a href="/easyloan">Easy Loan</a></div>
    </div>
    <nav class="menu">
    <#if loggedInUser?has_content>
    	<div class="menu-item"><a href="/easyloan/signout">Sign Out</a></div>
    	<div class="menu-item">Welcome ${loggedInUser.userName!}</div>
    <#else>	
      <div class="menu-item"><a href="/easyloan/signup">Sign Up</a></div>
       <div class="menu-item"><a href="/easyloan/signin">Login</a></div>
     </#if>  
      <div class="menu-item">About Us</div>
      <div class="menu-item">Apply For Personal Loan</div>
      <div class="menu-item"><a href="/easyloan/how-easyloan-works.html">How It Works?</a></div>
    </nav>
    <div class="clearfix">
    </div>
    
  </div>
</header>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/easyloan/static/css/style.css"> 
<#nested/>
<footer class="footer-under">
  <div class="container-width">
    <div class="footer-container">
      <div class="foot-lists">
        <div class="foot-list">
          <div class="foot-list-title">Company
          </div>
          <div class="foot-list-item">About Us
          </div>
          <div class="foot-list-item">Careers
          </div>
          <div class="foot-list-item">Blog
          </div>
          <div class="foot-list-item">Terms & Conditions
          </div>
          <div class="foot-list-item">Testimonials
          </div>
	<div class="foot-list-item">SiteMap
          </div>
	<div class="foot-list-item">Privacy Policy
          </div>
	<div class="foot-list-item">FAQ
          </div>
        </div>
        <div class="foot-list">
          <div class="foot-list-title">Contact Us
          </div>
          <div class="foot-list-item">Corporate Enquiry
          </div>
          <div class="foot-list-item">Facebook
          </div>
          <div class="foot-list-item">@NoBrokerIn
          </div>
          <div class="foot-list-item">Google+
          </div>
          <div class="foot-list-item">Crunchbase
          </div> 
          <div class="foot-list-item">Media
          </div>
        </div>
        <div class="clearfix">
        </div>
      </div>
      <div class="form-sub">
        <div class="foot-form-cont">
          <div class="foot-form-title">Subscribe
          </div>
          <div class="foot-form-desc">Subscribe to our newsletter to receive exclusive offers and the latest news
          </div>
          <input name="name" placeholder="Name" class="sub-input"/>
          <input name="email" placeholder="Email" class="sub-input"/>
          <button type="button" class="sub-btn">Submit</button>
        </div>
      </div>
    </div>
  </div>
  <div class="copyright">
    <div class="container-width">
      <div class="made-with">
      ©2013-17 NoBroker.in
      </div>
      <div class="foot-social-btns">facebook twitter linkedin mail
      </div>
      <div class="clearfix">
      </div>
    </div>
  </div>
</footer>
</#compress>
</#macro>