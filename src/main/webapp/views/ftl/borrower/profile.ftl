<@page>
<style>
	.invest{
		height: 30px;
	    margin: 0 10px 10px 0;
	    padding: 3px 5px;
	    border: .5px solid #e1e1e1;
	    border-radius: 100px;
	    font-size: 13px;
	    letter-spacing: .4px;
	    line-height: 29px;
	    color: red;
	    width: 100px;
	}
	.borderlist {
    	list-style-position:inside;
    	border: 1px solid black;
	}
	li {
		height: 50px;
	    width: 285px;
	    line-height: 50px;
	    border-bottom: 5px solid transparent;
	    background: #ddd;
	    display: inline-block;
	    text-align: center;
	    background-clip: padding-box;
	    margin-left: -55px;
	}
	
</style>
<div class="modal fade" id="paymentModal" data-backdrop="static" data-keyboard="true" tabindex="-1">
	<div class="modal-dialog">
    	<div class="modal-content flat-style">
    		<div class="modal-header modal-nobroker-header" style ="background-color: #fd3753;">
    			<button type="button" class="close" onclick="customizedAlertCallback(false)" data-dismiss="modal" aria-hidden="true" style="margin-top: 10px;"><i class="icon icon-remove-circle"></i></button>
    			<div class="row">
    				<div class="col-xs-9" style="margin-top: 4px;margin-left: 13px;">
    					<h4 class="modal-title">Pay Loan</h4>
    				</div>
    			</div>
    		</div><!-- /.modal-header -->
    		<div class="modal-body">
    			Enter Amout You want to Pay
    			 <input type="text" id="amount"><br>
    		</div><!-- /.modal-body -->
    		<div class="modal-footer" style="padding-top:5px;padding-bottom:5px">
    			<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
    			<button type="button" class="btn btn-success" onclick="payNow();">OK</button>
    		</div>
	  	</div><!-- /.modal-content -->
  	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="container" style="height:1000px;">
	<div class="col-xs-12">
		<div class="col-xs-4">
			<h4>${borrower.name!} Loan Request</h4>
		</div>
		<div class="col-xs-4">
			<h4>Credit Score 701-750 Good</h4>
		</div>
		<div class="col-xs-4">
			<h4>ROI 23.00% to 23.00% High</h4>
		</div>
	</div>
	<br />
	<input type="hidden" id="borrowerId" value="${borrower.id!}" />
	<input type="hidden" id="lenderId" value="${loggedInUser.id!}" />
	
	<div class="modal-body row vertical-align">
		<div class="col-sm-3 col-md-3 col-lg-3 col-xs-3" style="background-color: #efefef; height: 500px;">
			<ul style="list-style-type:none">
			  <li><h4>AUTO INVEST</h4></li>
			  <li><h4>AUTO INVEST HISTORY</h4></li>
			  <li><h4>NOTIFICATIONS</h4></li>
			  <li><h4>MESSAGES</h4></li>
			  <li><h4>ESCROW PASSBOOK</h4></li>
			  <li><h4>PORTFOLIO SUMMARY</h4></li>
			</ul>
		</div>
		<div class="col-sm-9 col-md-9 col-lg-9 col-xs-9">
			<table style="width:100%;background-color: #efefef;min-height: 60px;">
				<tr>
					<td>
						${borrower.amount!}
					</td>
					<td>
						${borrower.purpose!}
					</td>
					<td>
						${borrower.tenure!} Months
					</td>	
					<td>
						${borrower.funded!} Funded
					</td>
					<td>
						5 left to go		
					</td>
					<td rowspan="2">
						<button class="btn btn-primary" onclick="showModal();">Pay Now</button>
					</td>
				</tr>
				<tr>
					<td>
						LOAN AMOUNT
					</td>
					<td>
						PURPOSE
					</td>
					<td>
						TENURE
					</td>
					<td>
						${borrower.needed!} Needed
					</td>
					<td>
						${borrower.noOfProposals!} Proposals Recd.
					</td>			
				</tr>
			</table>
			<div style="height: 35px;"></div> 
			<h4>PROFILE SUMMARY</h4>
			<div class="col-xs-12" style="background-color:  lightblue;">
				<div class="col-xs-6">
					<table style="width:100%; min-height: 300px;">
						<tr>
							<td>
								Employment
							</td>
							<td>
								Self Employed
								Professional
							</td>
						</tr>
						<tr>
							<td>
								Age
							</td>
							<td>
								32 Years
							</td>
						</tr>
						<tr>
							<td>
								Gender
							</td>
							<td>
								MALE
							</td>
						</tr>
						<tr>
							<td>
								Residence Type
							</td>
							<td>
								Parental
							</td>
						</tr>
						<tr>
							<td>
								Marital Status
							</td>
							<td>
								Married
							</td>
						</tr>
						<tr>
							<td>
								No. of Dependents
							</td>
							<td>
								2
							</td>
						</tr>
					</table>
				</div>
				<div class="col-xs-3">
					<table style="width:100%; min-height: 300px;">
						<tr>
							<td>
								Education
							</td>
							<td>
								Graduate
							</td>
						</tr>
						<tr>
							<td>
								Designation
							</td>
							<td>
								Proprietor
							</td>
						</tr>
						<tr>
							<td>
								Co. Name
							</td>
							<td>
								Shreya Infocom
							</td>
						</tr>
						<tr>
							<td>
								Current Job Stability
							</td>
							<td>
								7 Years 0 Months
							</td>
						</tr>
						<tr>
							<td>
								Remarks
							</td>
							<td>
								In Home loan purchasing house
							</td>
						</tr>
					</table>
				</div>
				<div class="col-xs-3">
					<h4>Strengths +</h4>
					+ Parental House
					<br />
					<br />
					+ Business Registered
					<h4>Risk -</h4>
					+ Business Premises is rented
				</div>
			</div>

			<h4>Proposal Details</h4>
			<div class="col-x-12">
				<table style="width:100%; min-height: 100px;">
					<tr>
						<td>
							Name
						</td>
						<td>
							PROPOSED ON	
						</td>
						<td>
							PROPOSED
						</td>
						<td>
							ROI
						</td>
						<td>
							STATUS
						</td>
					</tr>
					<#list borrower.proposals as proposal>
						<tr>
							<td>
								${proposal.lender.userName!}
							</td>
							<td>
								${proposal.proposedOn!}
							</td>
							<td>
								${proposal.amount!}
							</td>
							<td>
								${proposal.ROI!}
							</td>
							<td>
								${proposal.status!}
							</td>
						</tr>
					</#list>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
		function showModal(){
			$("#paymentModal").modal("show");
		}
		function payNow(){
			var url = "/easyloan/api/v1/proposal/save";
			var params = {
				amount: $("#amount").val(),
				roi: 23,
				borrowerId: $("#borrowerId").val(),
				lenderId: $("#lenderId").val()
			};
			$.ajax ({
				url:url,
				type:"POST",
				data: params,
				success: function(resp){
					payURL = "http://localhost:8081/payment/initiatePayment?payment_params=c0e5f879f2793647b525b4a81ac2e6f296bd2b652051e0ad572d263c26499a423550c2551dcd4f1dd6eabecec7160b80d59b6e4d6c63d0a6f173355fdf07929bec693a6b034640cae7060c85bda3348b3b23c28cd6d0560ac53f57a0d45a997d316f605fd3c6fbc1acff308995b225af7ba19caba067d24f33c22a4988ed2746";
					window.open(payURL, '_blank');
					$("#paymentModal").modal("hide");
				},
				error:function(resp){
				}
			});
		}
	</script>
</@page>