<@page>
	<div class="container" style="height:600px;">
		<div class="row">
			<div class="col-xs-12">
				<table class="table table-striped table-bordered" style="width:100%">
					<thead style="width:100%">
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Amount</th>
						<th>Purpose</th>
						<th>Employment</th>
					</thead>
					<tfoot style="width:100%">
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Amount</th>
						<th>Purpose</th>
						<th>Employment</th>
					</tfoot>
					<tbody>
						<#if borrowerList?has_content>
							<#list borrowerList as borrower>
								<tr>
									<td>
										<a href="/easyloan/borrower/profile/${borrower.id!}">${borrower.name!}</a>
									</td>
									<td>
										${borrower.email!}
									</td>
									<td>
										${borrower.phone!}
									</td>
									<td>
										${borrower.amount!}
									</td>
									<td>
										${borrower.purpose!}
									</td>
									<td>
										${borrower.employment!}
									</td>
								</tr>
							</#list>
						</#if>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</@page>