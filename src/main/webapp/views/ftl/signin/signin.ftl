<@page>
<style>
	#header{
		position: relative;
	}
	a {
    	text-decoration: none;
    	color: #0645ad;
    	background: none;
	}
	.error {
		color: red;
	}
</style>
<div class="container" style="height:600px;">
	<#if errorMessage?has_content>
		<h4 class="error">${errorMessage!}</h4>
	</#if>
	<#if loggedInUser?has_content>
		<h3>
			Welcome ${loggedInUser.userName!}
		</h3>
	<#else>
		<h3>Sign In</h3>
		<form action="/easyloan/signin" method="post">
			<div class="col-xs-4">
				<div class="form-group">
					<label for="username">User Name:</label>
					 <input type="text" class="form-control" id="userName" name="userName" required>
				</div>
				<div class="form-group">
					<label for="password">Password:</label> <input type="password" class="form-control" id="password" name="password" required>
				</div>
				<div class="form-group">
					  <input type="submit" value="Login">
				</div>
			</div>
		</form>
	</#if> 
</div>	
</@page>