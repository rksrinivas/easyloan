<@page>
	<div class="container" style="height:600px;">
		<div class="row">
			<div class="col-xs-12">
				<table class="table table-striped table-bordered" style="width:100%">
					<thead style="width:100%">
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Amount</th>
						<th>Purpose</th>
						<th>Employment</th>
						<th>Action</th>
					</thead>
					<tfoot style="width:100%">
						<th>Name</th>
						<th>Email</th>
						<th>Phone</th>
						<th>Amount</th>
						<th>Purpose</th>
						<th>Employment</th>
						<th>Action</th>
					</tfoot>
					<tbody>
						<#if borrowerList?has_content>
							<#list borrowerList as borrower>
								<tr>
									<td>
										${borrower.name!}
									</td>
									<td>
										${borrower.email!}
									</td>
									<td>
										${borrower.phone!}
									</td>
									<td>
										${borrower.amount!}
									</td>
									<td>
										${borrower.purpose!}
									</td>
									<td>
										${borrower.employment!}
									</td>
									<td>
										<button class="btn" onclick="activate('${borrower.id!}');">Activate</button>
									</td>
								</tr>
							</#list>
						</#if>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function activate(id){
			var url = "/easyloan/api/v1/borrower/activate";
			var params = {
				id: id
			};
			$.ajax ({
				url:url,
				type:"POST",
				data: params,
				success: function(resp){
					alert("Customer Activated");
				},
				error:function(resp){
				}
			});
		}
	</script>
</@page>