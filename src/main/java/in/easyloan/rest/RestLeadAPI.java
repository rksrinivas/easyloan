package in.easyloan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import in.easyloan.domain.Borrower;
import in.easyloan.services.BorrowerService;

@Path("/v1/lead")
@Component
public class RestLeadAPI {
	
	@Autowired
	private BorrowerService borrowerService;
	
	@Path("/save")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces( MediaType.APPLICATION_JSON)
	@POST
	public Response saveLead(	@FormParam("name") String name,
								@FormParam("email") String email,
								@FormParam("phone") String phone,
								@FormParam("amount") Integer amount,
								@FormParam("purpose") String purpose,
								@FormParam("tenure") String tenure,
								@FormParam("employment") String employment,
								@FormParam("age") String age,
								@FormParam("gender") String gender,
								@FormParam("residenceType") String residenceType,
								@FormParam("maritalStatus") String maritalStatus,
								@FormParam("noOfDependents") String noOfDependents,
								@FormParam("education") String education,
								@FormParam("designation") String designation,
								@FormParam("currentJobStability") String currentJobStability,
								@FormParam("remarks") String remarks){
		
		Borrower borrower = new Borrower();
		borrower.setName(name);
		borrower.setEmail(email);
		borrower.setPhone(phone);
		borrower.setAmount(amount);
		borrower.setPurpose(purpose);
		borrower.setTenure(tenure);
		borrower.setEmployment(employment);
		borrower.setAge(age);
		borrower.setGender(gender);
		borrower.setResidenceType(residenceType);
		borrower.setMaritalStatus(maritalStatus);
		borrower.setNoOfDependents(noOfDependents);
		borrower.setEducation(education);
		borrower.setDesignation(designation);
		borrower.setCurrentJobStability(currentJobStability);
		borrower.setActive(false);
		
		borrowerService.saveOrUpdate(borrower);
		return Response.ok().build();
	}


}
