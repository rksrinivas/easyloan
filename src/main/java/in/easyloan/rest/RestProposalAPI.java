package in.easyloan.rest;

import java.util.Calendar;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import in.easyloan.domain.Borrower;
import in.easyloan.domain.Proposal;
import in.easyloan.domain.User;
import in.easyloan.services.BorrowerService;
import in.easyloan.services.ProposalService;
import in.easyloan.services.UserService;

@Path("/v1/proposal")
@Component
public class RestProposalAPI {

	@Autowired
	private ProposalService proposalService;
	
	@Autowired
	private BorrowerService borrowerService;
	
	@Autowired
	private UserService userService;
	
	@Path("/save")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces( MediaType.APPLICATION_JSON)
	@POST
	public Response saveProposal(	@FormParam("amount") Integer amount,
									@FormParam("roi") Integer roi,
									@FormParam("borrowerId") String borrowerId,
									@FormParam("lenderId") String lenderId
			){
		Borrower borrower = borrowerService.getById(borrowerId);
		User lender = userService.getById(lenderId);
		
		Proposal proposal = new Proposal();
		proposal.setAmount(amount);
		proposal.setBorrower(borrower);
		proposal.setLender(lender);
		proposal.setROI(roi);
		proposal.setProposedOn(Calendar.getInstance().getTime());
		proposal.setStatus("SUCCESS");
		
		proposalService.saveOrUpdate(proposal);
		
		Integer funded = borrower.getFunded();
		if(funded==null){
			funded = amount;
		}else{
			funded += amount;
		}
		borrower.setFunded(funded);
		
		Integer needed = borrower.getNeeded();
		if(needed == null){
			needed = borrower.getAmount() - amount; 
		}else{
			needed -= amount;
		}
		borrower.setNeeded(needed);
		
		Integer noOfProposals = borrower.getNoOfProposals();
		if(noOfProposals == null){
			noOfProposals = 0;
		}
		noOfProposals++;
		borrower.setNoOfProposals(noOfProposals);
		borrowerService.saveOrUpdate(borrower);
		
		return Response.ok().build();
	}
	
	
}
