package in.easyloan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import in.easyloan.domain.Borrower;
import in.easyloan.services.BorrowerService;

@Path("/v1/borrower")
@Component
public class RestBorrowerAPI {

	@Autowired
	private BorrowerService borrowerService;
	
	@Path("/activate")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces( MediaType.APPLICATION_JSON)
	@POST
	public Response activate(	@FormParam("id") String id){
		Borrower borrower = borrowerService.getById(id);
		borrower.setActive(true);
		borrowerService.saveOrUpdate(borrower);
		return Response.ok().build();
	}
			
}
