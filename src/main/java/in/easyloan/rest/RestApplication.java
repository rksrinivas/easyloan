package in.easyloan.rest;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

public class RestApplication extends ResourceConfig {
	public RestApplication() {
		super();
		packages("in.easyloan.rest");
		register(MultiPartFeature.class);
	}
}