package in.easyloan.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.easyloan.domain.User;

@Repository
public class UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public void saveOrUpdate(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
	}

	@Transactional(readOnly = true)
	public User getById(String id) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.setCacheable(true);
		criteria.add(Restrictions.eq("id", id));
		return (User) criteria.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<User> getByIds(List<String> ids) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.setCacheable(true);
		criteria.add(Restrictions.in("id", ids));
		return criteria.list();
	}

	@Transactional(readOnly = true)
	public User getByUserName(String userName) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.setCacheable(true);
		criteria.add(Restrictions.eq("userName", userName));
		return (User) criteria.uniqueResult();
	}

	@Transactional(readOnly = true)
	public User getUserByUserNameAndPassword(String userName, String password) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.setCacheable(true);
		criteria.add(Restrictions.eq("userName", userName));
		criteria.add(Restrictions.eq("password", password));
		return (User) criteria.uniqueResult();
	}

}
