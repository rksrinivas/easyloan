package in.easyloan.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.easyloan.domain.Proposal;

@Repository
public class ProposalDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public void saveOrUpdate(Proposal proposal) {
		sessionFactory.getCurrentSession().saveOrUpdate(proposal);
	}

}
