package in.easyloan.dao;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.easyloan.domain.Token;

@Repository
@Transactional
public class TokenDao {

	@Resource
	private SessionFactory sessionFactory;

	public void createNewToken(Token token) {
		sessionFactory.getCurrentSession().save(token);
	}

	public void updateToken(String series, String tokenValue, Date lastUsed) {

		Token existingToken = (Token) sessionFactory.getCurrentSession().get(Token.class, series);
		existingToken.setTokenValue(tokenValue);
		existingToken.setDate(lastUsed);
		sessionFactory.getCurrentSession().merge(existingToken);
	}

	public Token getTokenForSeries(String seriesId) {
		return (Token) sessionFactory.getCurrentSession().get(Token.class, seriesId);
	}

	@SuppressWarnings("unchecked")
	public void removeUserTokens(final String username) {

		List<Token> tokens = sessionFactory.getCurrentSession().createCriteria(Token.class)
				.add(Restrictions.eq("username", username)).list();

		for (Token token : tokens) {
			sessionFactory.getCurrentSession().delete(token);
		}

	}

	public void removeUserTokens(final String username, String series) {

		Token token = (Token) sessionFactory.getCurrentSession().createCriteria(Token.class)
				.add(Restrictions.eq("username", username)).add(Restrictions.eq("series", series)).uniqueResult();
		if (token == null) {
			return;
		}
		sessionFactory.getCurrentSession().delete(token);
	}
}