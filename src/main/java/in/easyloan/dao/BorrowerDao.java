package in.easyloan.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import in.easyloan.domain.Borrower;

@Repository
public class BorrowerDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Transactional
	public void saveOrUpdate(Borrower borrower) {
		sessionFactory.getCurrentSession().saveOrUpdate(borrower);
	}
	
	@Transactional(readOnly = true)
	public Borrower getById(String id) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Borrower.class);
		criteria.setCacheable(true);
		criteria.add(Restrictions.eq("id", id));
		return (Borrower) criteria.uniqueResult();
	}
	
	@Transactional(readOnly = true)
	public List<Borrower> getAllInActive() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Borrower.class);
		criteria.setCacheable(true);
		criteria.add(Restrictions.eq("active", false));
		return criteria.list();
	}
	
	@Transactional(readOnly = true)
	public List<Borrower> getAllActive() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Borrower.class);
		criteria.setCacheable(true);
		criteria.add(Restrictions.eq("active", true));
		return criteria.list();
	}
	
	
}
