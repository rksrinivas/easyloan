package in.easyloan.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;

public class StaticContentCacheFilter extends OncePerRequestFilter {

	public static final String STATIC_CONTENT_KEY = "X-NB-CONTENT-TYPE";
	public static final String STATIC_CONTENT_TRUE = "X-NB-CONTENT-TRUE";

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain fc)
			throws ServletException, IOException {
		fc.doFilter(request, response);
		((HttpServletResponse) response).addHeader(STATIC_CONTENT_KEY, STATIC_CONTENT_TRUE);

	}

}
