package in.easyloan.util;

import javax.servlet.http.HttpServletResponse;

import in.easyloan.filter.StaticContentCacheFilter;

public class WebUtils {

	public static boolean isStaticRequest( HttpServletResponse response){
		String headerValue = response.getHeader( StaticContentCacheFilter.STATIC_CONTENT_KEY );
		if( StaticContentCacheFilter.STATIC_CONTENT_TRUE.equals(headerValue)){
			return true;
		}
		return false;
	}
}
