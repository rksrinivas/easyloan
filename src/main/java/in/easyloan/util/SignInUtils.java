package in.easyloan.util;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.stereotype.Component;

import in.easyloan.bean.UserDetails;
import in.easyloan.domain.User;

@Component
public class SignInUtils {

	private static RememberMeServices rememberMeServices;

	@Autowired(required = true)
	public void setUserAccessor(RememberMeServices services) {
		SignInUtils.rememberMeServices = services;
	}

	@SuppressWarnings("deprecation")
	public static void signin(User loggedInUser, HttpServletResponse response, HttpServletRequest request) {

		GrantedAuthority[] authorities = new GrantedAuthorityImpl[0];

		UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(
				new UserDetails(loggedInUser), loggedInUser.getPassword(), Arrays.asList(authorities));

		SecurityContextHolder.getContext().setAuthentication(authRequest);

		rememberMeServices.loginSuccess(request, response, SecurityContextHolder.getContext().getAuthentication());
	}
}
