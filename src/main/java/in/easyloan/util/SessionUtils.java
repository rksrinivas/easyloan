package in.easyloan.util;

import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import in.easyloan.bean.UserDetails;
import in.easyloan.domain.User;

public class SessionUtils {
	public static boolean isUserAnonymous() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		boolean authenticated = auth != null && !(auth instanceof AnonymousAuthenticationToken) && auth.isAuthenticated();
		return !authenticated;
	}
	
	public static boolean isUserAuthenicated() {
		return !isUserAnonymous();
	}
	
	public static User getLoggedInUser() {
		User u = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (isUserAuthenicated()) {
			u = ((UserDetails) auth.getPrincipal()).getUser();
		}
		
		return u;
	}
	
	public static HttpSession session() {
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest().getSession(true); // true == allow create
	}
}
