package in.easyloan.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.stereotype.Component;

@Component
public class AppUtils {

	public static final String JS_PATH = "js_path";
	public static final String CSS_PATH = "css_path";
	public static final String IMG_PATH = "img_path";

	public static final String getJSPath() {
		return "/easyloan/static/js";
	}

	public static final String getCSSPath() {
		return "/easyloan/static/css";
	}

	public static final String getImagePath() {
		return "/easyloan/static/img";
	}

	public static final String getImageLocation() {
		return "/Users/rk/images/";
	}

	public static void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) throws IOException {

		try {
			OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];

			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
			System.out.println("File uploaded successfully at " + uploadedFileLocation);
		} catch (IOException e) {
			System.out.print(e);
			throw (e);
		}

	}
}
