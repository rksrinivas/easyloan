package in.easyloan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import in.easyloan.domain.Borrower;
import in.easyloan.services.BorrowerService;

@RequestMapping("/admin")
@Controller
public class AdminController {

	@Autowired
	private BorrowerService borrowerService;
	
	@RequestMapping(value = { "", "/", "/index" }, method = RequestMethod.GET)
	public String index(ModelMap model) {
		List<Borrower> borrowerList = borrowerService.getAllInActive();
		model.addAttribute("borrowerList", borrowerList);
		return "admin/index";
	}
}
