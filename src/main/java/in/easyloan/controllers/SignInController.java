package in.easyloan.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import in.easyloan.domain.User;
import in.easyloan.services.UserService;
import in.easyloan.util.SignInUtils;


@Controller
public class SignInController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public String signinGet(ModelMap model) {
		return "signin/signin";
	}

	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	public String signinPost(ModelMap model, @RequestParam(value = "userName", required = false) String userName,
			@RequestParam(value = "password", required = false) String password, HttpServletRequest request,
			HttpServletResponse response) {

		User user = userService.getUserByUserNameAndPassword(userName, password);
		if (user == null) {
			User temp = userService.getByUserName(userName);
			if (temp != null) {
				String errorMessage = "You have entered wrong password";
				model.addAttribute("errorMessage", errorMessage);
				return "signin/signin";
			} else {
				String errorMessage = "Please enter correct user name";
				model.addAttribute("errorMessage", errorMessage);
				return "signin/signin";
			}
		} else {
			SignInUtils.signin(user, response, request);
			return "redirect:/index";
			// return "signin/signin";
		}
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signupGet(ModelMap model, HttpServletRequest request) {
		return "signin/signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signupPost(ModelMap model, @RequestParam(value = "userName", required = false) String userName,
			@RequestParam(value = "password", required = false) String password, HttpServletRequest request,
			HttpServletResponse response) {

		User user = userService.createAccount(userName, password);
		if (user == null) {
			String errorMessage = "User already exists with " + userName;
			model.addAttribute("errorMessage", errorMessage);
			return "signin/signup";
		} else {
			SignInUtils.signin(user, response, request);
			return "redirect:/index";
			// return "signin/signup";
		}
	}
}
