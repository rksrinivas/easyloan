package in.easyloan.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import in.easyloan.domain.Borrower;
import in.easyloan.services.BorrowerService;

@RequestMapping("/borrower")
@Controller
public class BorrowerController {

	@Autowired
	private BorrowerService borrowerService;
	
	@RequestMapping(value = { "/profile/{id}" }, method = RequestMethod.GET)
	public String profile(@PathVariable("id") String borrowerId, ModelMap model) {
		Borrower borrower = borrowerService.getById(borrowerId);
		model.put("borrower", borrower);
		return "borrower/profile";
	}
}
