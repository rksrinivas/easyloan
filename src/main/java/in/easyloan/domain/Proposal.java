package in.easyloan.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "user_proposal")
public class Proposal {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid")
	private String id;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "borrower_id", nullable = false)
	private Borrower borrower;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "lender_id", nullable = false)
	private User lender;
	
	@Column(name = "proposed_on", nullable = false)
	private Date proposedOn;
	
	@Column(name = "amount")
	private Integer amount;
	
	@Column(name = "roi")
	private Integer ROI;
	
	@Column(name = "status")
	private String  status;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Borrower getBorrower() {
		return borrower;
	}

	public void setBorrower(Borrower borrower) {
		this.borrower = borrower;
	}

	public User getLender() {
		return lender;
	}

	public void setLender(User lender) {
		this.lender = lender;
	}

	public Date getProposedOn() {
		return proposedOn;
	}

	public void setProposedOn(Date proposedOn) {
		this.proposedOn = proposedOn;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getROI() {
		return ROI;
	}

	public void setROI(Integer rOI) {
		ROI = rOI;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
