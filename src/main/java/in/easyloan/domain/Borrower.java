package in.easyloan.domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "Borrower")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "hibernateEntityCache")
public class Borrower {
	
	@Id
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@GeneratedValue(generator = "uuid")
	private String id;
	
	@Column(name = "active", columnDefinition="BIT")
	private Boolean active;
	
	@Column(name = "user_name", length = 255, unique = true, insertable = true, updatable = false)
	private String userName;

	@Column(name = "password", length = 255)
	private String password;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "phone")
	private String phone;
	
	@Column(name = "amount")
	private Integer amount;

	@Column(name = "purpose")
	private String purpose;
	
	@Column(name = "tenure")
	private String tenure;
	
	@Column(name = "days")
	private Integer days;

	@Column(name = "employment")
	private String employment;
	
	@Column(name = "age")
	private String age;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "residence_type")
	private String residenceType;
	
	@Column(name = "marital_status")
	private String maritalStatus;
	
	@Column(name = "no_of_dependents")
	private String noOfDependents;
	
	@Column(name = "education")
	private String education;
	
	@Column(name = "designation")
	private String designation;
	
	@Column(name = "current_job_stability")
	private String currentJobStability;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "funded")
	private Integer funded;
	
	@Column(name = "remaining_days")
	private Integer remainingDays;
	
	@Column(name = "needed")
	private Integer needed;
	
	@Column(name = "no_of_proposals")
	private Integer noOfProposals;
	
	@OneToMany(mappedBy="borrower",fetch=FetchType.EAGER)
	private List<Proposal> proposals;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getTenure() {
		return tenure;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public String getEmployment() {
		return employment;
	}

	public void setEmployment(String employment) {
		this.employment = employment;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getResidenceType() {
		return residenceType;
	}

	public void setResidenceType(String residenceType) {
		this.residenceType = residenceType;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getNoOfDependents() {
		return noOfDependents;
	}

	public void setNoOfDependents(String noOfDependents) {
		this.noOfDependents = noOfDependents;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getCurrentJobStability() {
		return currentJobStability;
	}

	public void setCurrentJobStability(String currentJobStability) {
		this.currentJobStability = currentJobStability;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getFunded() {
		return funded;
	}

	public void setFunded(Integer funded) {
		this.funded = funded;
	}

	public Integer getRemainingDays() {
		return remainingDays;
	}

	public void setRemainingDays(Integer remainingDays) {
		this.remainingDays = remainingDays;
	}

	public Integer getNeeded() {
		return needed;
	}

	public void setNeeded(Integer needed) {
		this.needed = needed;
	}

	public Integer getNoOfProposals() {
		return noOfProposals;
	}

	public void setNoOfProposals(Integer noOfProposals) {
		this.noOfProposals = noOfProposals;
	}

	public List<Proposal> getProposals() {
		return proposals;
	}

	public void setProposals(List<Proposal> proposals) {
		this.proposals = proposals;
	}
	
	
}
