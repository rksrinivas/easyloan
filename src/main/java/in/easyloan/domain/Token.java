package in.easyloan.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

@Entity
@Table(name = "persistent_logins")
public class Token {

	private String username;
	@Id
	private String series;

	@Column(name = "token")
	private String tokenValue;

	@Column(name = "last_used")
	private Date date;

	public Token() {
	}

	public Token(PersistentRememberMeToken persistentRememberMeToken) {
		this.username = persistentRememberMeToken.getUsername();
		this.series = persistentRememberMeToken.getSeries();
		this.date = persistentRememberMeToken.getDate();
		this.tokenValue = persistentRememberMeToken.getTokenValue();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getTokenValue() {
		return tokenValue;
	}

	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
