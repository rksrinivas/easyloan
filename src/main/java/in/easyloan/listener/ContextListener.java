package in.easyloan.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.context.ContextLoaderListener;

public class ContextListener extends ContextLoaderListener {

	@Override
	protected void configureAndRefreshWebApplicationContext(ConfigurableWebApplicationContext applicationContext,
			ServletContext arg1) {
		super.configureAndRefreshWebApplicationContext(applicationContext, arg1);
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	}
}
