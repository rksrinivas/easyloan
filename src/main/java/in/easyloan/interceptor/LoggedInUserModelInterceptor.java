package in.easyloan.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import in.easyloan.domain.User;
import in.easyloan.util.AppUtils;
import in.easyloan.util.SessionUtils;
import in.easyloan.util.WebUtils;


/*
 * Adds header related context as requestAttributes. FreemarkerView is p:p:exposeRequestAttributes=true.
 */
public class LoggedInUserModelInterceptor extends HandlerInterceptorAdapter {



	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);

		if (WebUtils.isStaticRequest(response)) {
			return;
		}
		request.setAttribute(AppUtils.JS_PATH, AppUtils.getJSPath());
		request.setAttribute(AppUtils.CSS_PATH, AppUtils.getCSSPath());
		request.setAttribute(AppUtils.IMG_PATH, AppUtils.getImagePath());
		
		if (SecurityContextHolder.getContext() != null
				&& SecurityContextHolder.getContext().getAuthentication() != null) {
			request.setAttribute("loggedIn", SessionUtils.isUserAuthenicated());
			if (SessionUtils.isUserAuthenicated()) {
				User loggedInUser = SessionUtils.getLoggedInUser();
				request.setAttribute("loggedInUser", loggedInUser);
				HttpSession session = SessionUtils.session();
				session.setAttribute("loggedInUserName", loggedInUser.getUserName());
			}
			request.setAttribute("principal", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
		}
	}
}
