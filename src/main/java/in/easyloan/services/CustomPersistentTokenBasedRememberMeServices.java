package in.easyloan.services;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;

@SuppressWarnings("deprecation")
public class CustomPersistentTokenBasedRememberMeServices extends PersistentTokenBasedRememberMeServices {

	@Autowired
	CustomTokenService tokenService;

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		cancelCookie(request, response);

		Cookie[] cs = request.getCookies();

		if (authentication != null) {
			for (Cookie c : cs) {
				if (SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY.equals(c.getName())) {
					String[] tokens = decodeCookie(c.getValue());
					if (tokens.length == 2) {
						String series = tokens[0];
						tokenService.removeUserTokens(authentication.getName(), series);
					}
				}
			}
		}
	}

	@Override
	protected Authentication createSuccessfulAuthentication(HttpServletRequest request, UserDetails user) {
		Authentication authentication = super.createSuccessfulAuthentication(request, user);
		return authentication;
	}

	@Override
	protected void onLoginSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) {
		super.onLoginSuccess(request, response, authentication);
	}
}
