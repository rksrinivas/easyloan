package in.easyloan.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import in.easyloan.bean.UserDetails;
import in.easyloan.dao.UserDao;
import in.easyloan.domain.User;



@Component("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserDao userDao;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		User user = userDao.getByUserName(userName);
		if (user == null) {
			throw new UsernameNotFoundException(userName + " not found");
		}
		UserDetails userDetails = new UserDetails(user);
		userDetails.getAuthorities().add(new SimpleGrantedAuthority("ROLE_USER") );

		return userDetails;
	}
}
