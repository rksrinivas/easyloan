package in.easyloan.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.easyloan.dao.BorrowerDao;
import in.easyloan.domain.Borrower;

@Service
public class BorrowerService {
	
	@Autowired
	private BorrowerDao borrowerDao;

	public void saveOrUpdate(Borrower borrower) {
		borrowerDao.saveOrUpdate(borrower);
	}

	public Borrower getById(String id) {
		return borrowerDao.getById(id);
	}
	
	public List<Borrower> getAllInActive(){
		return borrowerDao.getAllInActive();
	}
	
	public List<Borrower> getAllActive(){
		return borrowerDao.getAllActive();
	}
}
