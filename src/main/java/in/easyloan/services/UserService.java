package in.easyloan.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.easyloan.dao.UserDao;
import in.easyloan.domain.User;

@Service
public class UserService {

	@Autowired
	private UserDao userDao;

	public void saveOrUpdate(User user) {
		userDao.saveOrUpdate(user);
	}

	public User getById(String id) {
		return userDao.getById(id);
	}

	public List<User> getByIds(List<String> ids) {
		return userDao.getByIds(ids);
	}

	public User getByUserName(String userName) {
		return userDao.getByUserName(userName);
	}

	public User getUserByUserNameAndPassword(String userName, String password) {
		return userDao.getUserByUserNameAndPassword(userName, password);
	}

	public User createAccount(String userName, String password) {
		User user = userDao.getByUserName(userName);
		if (user != null) {
			// User exists by that username, do not create new account
			return null;
		} else {
			user = new User();
		}
		user.setUserName(userName);
		user.setPassword(password);
		userDao.saveOrUpdate(user);
		return user;
	}
}
