package in.easyloan.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.easyloan.dao.ProposalDao;
import in.easyloan.domain.Proposal;

@Service
public class ProposalService {

	@Autowired
	private ProposalDao proposalDao;

	public void saveOrUpdate(Proposal proposal) {
		proposalDao.saveOrUpdate(proposal);
	}
}
